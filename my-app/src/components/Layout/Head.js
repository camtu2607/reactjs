import React, {Component} from "react";
import {Link} from "react-router-dom"
class Head extends Component{
    render(){
        return(
            <div>
                <Link to="/Home">HOME</Link>
                <Link to="/Account">ACCOUNT</Link>
                <Link to="/Login">LOGIN</Link>
            </div>
        )
    }
}

export default Head