import './App.css';
import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import Head from './components/Layout/Head'
import Footer from './components/Layout/Footer'



class App extends Component{
  constructor(props){
    super(props)
  }

  render(){
    return(
            <div>
                <Head/>
                    {this.props.children}
                <Footer/>
            </div>
    )
  }
}

export default withRouter(App);
