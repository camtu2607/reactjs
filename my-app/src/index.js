import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  // Link,
  // // useRouteMatch
}from "react-router-dom";
import "./index.css";
import App from './App';
import Home from './components/Home';
import Account from './components/Account/Index';
import Login from './components/Member/Login';


ReactDOM.render(
  <div>
    <Router>
      <App>
        <Switch>
          <Route exact path= '/' component={Home}/>
          <Route path= '/Login' component={Login}/>
          <Route path= '/Account' component={Account}/>
        </Switch>
      </App>
    </Router>
  </div>,
  document.getElementById('root'))

