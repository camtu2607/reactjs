import React from 'react';
import ReactDOM from 'react-dom';
import{
  BrowserRouter as Router,
  Switch,
  Route,
}from "react-router-dom";
import './index.css';
import App from './App';
import home from './home';
import checkout from './checkout';
import login from './login';

ReactDOM.render(
  <div>
    <Router>
      <App>
        <Switch>
          <Route exact path = '/' component = {home}/>
          <Route exact path = '/checkout' component = {checkout}/>
          <Route exact path = '/login' component = {login}/>
        </Switch>
      </App>
    </Router>
  </div>,
  document.getElementById('root')
);
